package com.aftermoonest.operations;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Class used to found fibonacci sequence
 *
 * @author Alexey Kovalskiy (aftermoonest)
 */
public class Fibonacci {

    /**
     * Method found the most bigger odd number
     *
     * @param array - array
     * @return max number
     * .
     */
    private static int foundBiggestOddNumber(ArrayList<Integer> array) {
        ArrayList<Integer> arrayOdd = Number.findOdd(array);
        int max = arrayOdd.get(0);
        for (int i = 0; i < arrayOdd.size(); i++) {
            if (max < arrayOdd.get(i)) {
                max = arrayOdd.get(i);
            }
        }
        System.out.println("Max odd number: " + max);
        return max;
    }

    /**
     * Method found the most bigger even number
     *
     * @param array - array
     * @return max number
     * .
     */
    private static int foundBiggestEvenNumber(ArrayList<Integer> array) {
        ArrayList<Integer> arrayEven = Number.findEven(array);
        int max = arrayEven.get(0);
        for (int i = 0; i < arrayEven.size(); i++) {
            if (max < arrayEven.get(i)) {
                max = arrayEven.get(i);
            }
        }

        System.out.println("Max even number: " + max);
        return max;
    }

    /**
     * Method do count of fibonacci elements
     *
     * @param array - array
     * @param countOfElements - common count of elements
     */
    private static void countFibonacci(ArrayList<Integer> array, int countOfElements) {
        int countOfEvenElements = 0;
        int countOfOddElements = 0;

        int fibonacciElement1 = foundBiggestOddNumber(array);
        int fibonacciElement2 = foundBiggestEvenNumber(array);
        int fibonacciElement3;
        System.out.print("[ " + fibonacciElement1 + " " + fibonacciElement2 + " ");
        for (int i = 3; i <= countOfElements; i++) {
            fibonacciElement3 = fibonacciElement1 + fibonacciElement2;
            if (fibonacciElement3 % 2 == 0) {
                countOfEvenElements++;
            } else {
                countOfOddElements++;
            }
            System.out.print(fibonacciElement3 + " ");
            fibonacciElement1 = fibonacciElement2;
            fibonacciElement2 = fibonacciElement3;
        }
        System.out.print("]");
        countPercents(countOfEvenElements, countOfOddElements, countOfElements - 2);
    }

    /**
     * Method count percents
     * and print it.
     * @param countOfEven - count of all even numbers
     * @param countOfOdd - count of all odd numbers
     * @param countOfElements - count of all elements
     */
    private static void countPercents(int countOfEven, int countOfOdd, int countOfElements) {
        double persOfEven = (double) (countOfEven * 100) / countOfElements;
        persOfEven = (double) Math.round(persOfEven * 10) / 10;
        double persOfOdd = (double) (countOfOdd * 100) / countOfElements;
        persOfOdd = (double) Math.round(persOfOdd * 10) / 10;
        System.out.println();
        System.out.println("Percents of odd numbers: " + persOfOdd);
        System.out.println("Percents of even numbers: " + persOfEven);
    }

    /**
     * Method enter count of elements
     * and call countFibonacci() method.
     * @param array - array
     */
    public static void findFibonacci(ArrayList<Integer> array) {
        int countOfElements;
        Scanner in;
        while (true) {
            System.out.print("Enter interval of fibonacci: ");
            in = new Scanner(System.in);
            if (in.hasNextInt()) {
                countOfElements = in.nextInt();
                break;
            } else {
                System.out.print("Wrong number");
            }
        }
        countFibonacci(array, countOfElements);
    }
}
