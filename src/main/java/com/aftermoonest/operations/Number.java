package com.aftermoonest.operations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Class have stock array.
 * Class takes actions on number array.
 * Also have methods findOdd(), findEven(), findSum()
 *
 * @author Alexey Kovalskiy (aftermoonest)
 */
public class Number {

    /**
     * Constructor initialize array
     * and filing it out
     */
    public Number() {
        enterInterval();
    }

    /**
     * Array field
     */
    private ArrayList<Integer> array;

    /**
     * Getter for array
     *
     * @return array
     * .
     */
    public ArrayList<Integer> getArray() {
        return array;
    }

    /**
     * Start point of interval
     */
    private int intervalFrom;

    /**
     * End point of interval
     */
    private int intervalTo;

    /**
     * Enter interval from console
     * and call method fillArray()
     */
    private void enterInterval() {
        Scanner in;
        String input;
        while (true) {
            System.out.print("Enter interval from: ");
            in = new Scanner(System.in);
            if (in.hasNextInt()) {
                intervalFrom = in.nextInt();
                break;
            } else {
                System.out.print("Wrong number");
            }
        }

        while (true) {
            System.out.print("Enter interval to: ");
            in = new Scanner(System.in);
            if (in.hasNextInt()) {
                intervalTo = in.nextInt();
                break;
            } else {
                System.out.print("Wrong number");
            }
        }

        fillArray();
    }

    /**
     * Create range from intervalTo to intervalFrom
     * and fill array by increment
     */
    private void fillArray() {
        int length = intervalTo - intervalFrom + 1;
        array = new ArrayList<>();
        int counter = intervalFrom;
        for (int i = 0; i < length; i++) {
            array.add(counter);
            counter++;
        }
        System.out.println(Arrays.toString(array.toArray()));
    }

    /**
     * Method find all odd numbers in array
     *
     * @param array in which need to find odd numbers
     * @return array with odd numbers
     * .
     */
    static ArrayList<Integer> findOdd(ArrayList<Integer> array) {
        ArrayList<Integer> arrayOdd = new ArrayList<>();
        for (int i = 0; i < array.size(); i++) {
            if (array.get(i) % 2 != 0) {
                arrayOdd.add(array.get(i));
            }
        }
        return arrayOdd;
    }

    /**
     *  Method find all even numbers in array
     *
     *  @param array in which need to find even numbers
     *  @return array with even numbers
     *  .
     */
    static ArrayList<Integer> findEven(ArrayList<Integer> array) {
        ArrayList<Integer> arrayOdd = new ArrayList<>();
        for (int i = 0; i < array.size(); i++) {
            if (array.get(i) % 2 == 0) {
                arrayOdd.add(array.get(i));
            }
        }
        return arrayOdd;
    }

    /**
     * Method find sum of all elements in array
     *
     * @param array in which need to find sum
     * @return sum
     * .
     */
    static int findSum(ArrayList<Integer> array) {
        int sum = 0;
        for (int i = 0; i < array.size(); i++) {
            sum += array.get(i);
        }
        return sum;
    }
}
