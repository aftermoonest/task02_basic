package com.aftermoonest.operations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Class Print are able to print arrays
 * Have methods printOddEvenNumbers and printSum.
 *
 *
 * @author Alexey Kovalskiy (aftermoonest)
 */
public class Print {

    /**
     * Method is used for print odd and even number
     * of array. Odd and even numbers are taken from findOdd() method
     * and find even method.
     *
     * @param array is array which need to print
     */
    public static void printOddEvenNumbers(ArrayList<Integer> array) {
        System.out.println("Odd array:\n" + Arrays.toString(Number.findOdd(array).toArray()));
        Collections.reverse(array);
        System.out.println("Even reverse array:\n" + Arrays.toString(Number.findEven(array).toArray()));
    }

    /**
     * Method is used for print sum of odd and even numbers of
     * array
     *
     * @param array is array which need to print
     */
    public static void printSum(ArrayList<Integer> array) {
        System.out.println("Sum of odd numbers = " + Number.findSum(Number.findOdd(array)));
        System.out.println("Sum of even numbers = " + Number.findSum(Number.findEven(array)));
    }
}
