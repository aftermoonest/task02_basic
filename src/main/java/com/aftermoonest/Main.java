package com.aftermoonest;

import com.aftermoonest.operations.Fibonacci;
import com.aftermoonest.operations.Number;
import com.aftermoonest.operations.Print;

public class Main {
    public static void main(String[] args) {
        Number number = new Number();
        Print.printOddEvenNumbers(number.getArray());
        Print.printSum(number.getArray());
        Fibonacci.findFibonacci(number.getArray());
    }
}
